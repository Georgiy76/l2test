import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class LoginLogout {
    WebDriver wb = new ChromeDriver();

    @BeforeTest
    public void beforeClass(){
        wb.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        wb.manage().window().fullscreen();
    }

    @Test
    public void test() throws InterruptedException {
        //log in
        wb.findElement(By.id("email")).sendKeys("webinar.test@gmail.com");
        wb.findElement(By.id("passwd")).sendKeys("Xcg7299bnSmMuRLp9ITw");
        wb.findElement(By.cssSelector("#login_form > div.form-group.row-padding-top > button")).submit();

        Thread.sleep(3000);

        //log out
        wb.findElement(By.xpath("//*[@id=\"employee_infos\"]")).click();
        wb.findElement(By.xpath("//*[@id=\"header_logout\"]")).click();

        Thread.sleep(2000);




    }

    @AfterTest
    public void afterClass(){
        wb.quit();
    }
}
