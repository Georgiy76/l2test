import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class MainMenu {
    WebDriver wb = new ChromeDriver();

    @BeforeTest
    public void login(){
        wb.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        wb.manage().window().fullscreen();
        Assert.assertEquals("prestashop-automation > Панель администратора (PrestaShop™)", wb.getTitle());
    }

    @BeforeClass
    public void beforeClass() throws InterruptedException{

        wb.findElement(By.id("email")).sendKeys("webinar.test@gmail.com");
        wb.findElement(By.id("passwd")).sendKeys("Xcg7299bnSmMuRLp9ITw");
        wb.findElement(By.cssSelector("#login_form > div.form-group.row-padding-top > button")).submit();
        Thread.sleep(1000);
        Assert.assertEquals("Dashboard • prestashop-automation", wb.getTitle());
    }

    @Test
    public void test(){

        //заказы
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminParentOrders\"]")).click();
        Assert.assertEquals("Заказы • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminInvoices\"]")).click();
        Assert.assertEquals("Инвойсы • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminSlip\"]")).click();
        Assert.assertEquals("Кредитные счета • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminDeliverySlip\"]")).click();
        Assert.assertEquals("Счета на доставку • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminCarts\"]")).click();
        Assert.assertEquals("Корзины • prestashop-automation", wb.getTitle());


        //каталог
        wb.findElement(By.id("subtab-AdminCatalog")).click();
        Assert.assertEquals("товары • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("/html/body/nav/ul/li[4]/ul/li[2]")).click();
        Assert.assertEquals("категории • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminTracking\"]")).click();
        Assert.assertEquals("Мониторинг • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminParentAttributesGroups\"]")).click();
        Assert.assertEquals("Атрибуты и комбинации > Атрибуты и комбинации • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminParentManufacturers\"]")).click();
        Assert.assertEquals("производители • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminAttachments\"]")).click();
        Assert.assertEquals("Загрузить • prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("//*[@id=\"subtab-AdminParentCartRules\"]")).click();
        Assert.assertEquals("Правила корзины • prestashop-automation", wb.getTitle());

//        for (WebElement webElement : wb.findElements(By.cssSelector("body > nav > ul > li.link-levelone.-active > ul > li"))) {
//            webElement.click();
//        }

        //клиенты
        wb.findElement(By.id("subtab-AdminParentCustomer")).click();
        Assert.assertEquals("Управление клиентами • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminAddresses")).click();
        Assert.assertEquals("Адреса • prestashop-automation", wb.getTitle());

        //служба поддержки
        wb.findElement(By.id("subtab-AdminParentCustomerThreads")).click();
        Assert.assertEquals("Customer Service • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminOrderMessage")).click();
        Assert.assertEquals("Сообщения • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminReturn")).click();
        Assert.assertEquals("Возвраты товаров • prestashop-automation", wb.getTitle());

        //статистика
        wb.findElement(By.id("subtab-AdminStats")).click();
        Assert.assertEquals("Статистика • prestashop-automation", wb.getTitle());

        //modules
        wb.findElement(By.id("subtab-AdminParentModulesSf")).click();
        Assert.assertEquals("prestashop-automation", wb.getTitle());
        wb.findElement(By.xpath("/html/body/nav/ul/li[9]/ul/li[2]")).click();
        Assert.assertEquals("Modules Catalog • prestashop-automation", wb.getTitle());

        //design
        wb.findElement(By.xpath("/html/body/nav/ul/li[10]")).click();
        Assert.assertEquals("Шаблоны • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminThemesCatalog")).click();
        Assert.assertEquals("Theme Catalog • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminCmsContent")).click();
        Assert.assertEquals("Pages • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminModulesPositions")).click();
        Assert.assertEquals("Расположение • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminImages")).click();
        Assert.assertEquals("Image Settings • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminLinkWidget")).click();
        Assert.assertEquals("Блок ссылок • prestashop-automation", wb.getTitle());

        //доставка
        wb.findElement(By.id("subtab-AdminParentShipping")).click();
        Assert.assertEquals("Курьеры • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminShipping")).click();
        Assert.assertEquals("Настройки • prestashop-automation", wb.getTitle());

        //способ оплаты
        wb.findElement(By.id("subtab-AdminParentPayment")).click();
        Assert.assertEquals("Payment Methods • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminPaymentPreferences")).click();
        Assert.assertEquals("Preferences • prestashop-automation", wb.getTitle());

        //interntaional
        wb.findElement(By.id("subtab-AdminInternational")).click();
        Assert.assertEquals("Локализация • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentCountries")).click();
        Assert.assertEquals("Страны • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentTaxes")).click();
        Assert.assertEquals("Налоги • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminTranslations")).click();
        Assert.assertEquals("Переводы • prestashop-automation", wb.getTitle());

        //shop parameters
        wb.findElement(By.id("subtab-ShopParameters")).click();
        Assert.assertEquals("General • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentOrderPreferences")).click();
        Assert.assertEquals("Order Settings • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminPPreferences")).click();
        Assert.assertEquals("товары • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentCustomerPreferences")).click();
        Assert.assertEquals("Клиенты • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentStores")).click();
        Assert.assertEquals("Контакты • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentMeta")).click();
        Assert.assertEquals("SEO и URLs • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentSearchConf")).click();
        Assert.assertEquals("Поиск • prestashop-automation", wb.getTitle());

        //конфигурация
        wb.findElement(By.id("subtab-AdminAdvancedParameters")).click();
        Assert.assertEquals("Information • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminPerformance")).click();
        Assert.assertEquals("Результат • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminAdminPreferences")).click();
        Assert.assertEquals("Администрация • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminEmails")).click();
        Assert.assertEquals("E-mail • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminImport")).click();
        Assert.assertEquals("Импорт • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentEmployees")).click();
        Assert.assertEquals("Employees • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminParentRequestSql")).click();
        Assert.assertEquals("Менеджер SQL • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminLogs")).click();
        Assert.assertEquals("Журналы • prestashop-automation", wb.getTitle());
        wb.findElement(By.id("subtab-AdminWebservice")).click();
        Assert.assertEquals("WEB службы • prestashop-automation", wb.getTitle());

    }

    @AfterClass
    public void afterClass() throws InterruptedException {
        wb.findElement(By.id("employee_infos")).click();
        wb.findElement(By.xpath("//*[@id=\"header_logout\"]")).click();
        Assert.assertEquals("prestashop-automation > Панель администратора (PrestaShop™)", wb.getTitle());
        Thread.sleep(2000);
    }

    @AfterTest
    public void afterTest(){
        wb.quit();
    }

}

